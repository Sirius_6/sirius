import { Component, OnInit } from '@angular/core';
import{Router} from '@angular/router'
@Component({
  selector: 'app-gradepage',
  templateUrl: './gradepage.component.html',
  styleUrls: ['./gradepage.component.css']
})
export class GradepageComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit(): void {
  }
  getgrade5(){
    this.router.navigate(['/grade5']);
  }

}
