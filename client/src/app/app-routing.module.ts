import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import{LoginComponent} from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ShowuserComponent } from './showuser/showuser.component';
import { HomepageComponent } from './homepage/homepage.component';
import { HeaderComponent } from './header/header.component';
import { CurriculumComponent } from './curriculum/curriculum.component';
import { GradepageComponent } from './gradepage/gradepage.component';
import { Grade1Component } from './grade1/grade1.component';
import { Grade2Component } from './grade2/grade2.component';
import { Grade3Component } from './grade3/grade3.component';
import { Grade4Component } from './grade4/grade4.component';
import { Grade5Component } from './grade5/grade5.component';
import { ExtraCurricularComponent } from './extra-curricular/extra-curricular.component';
import { GamepageComponent } from './gamepage/gamepage.component';
import { CrispgameComponent } from './crispgame/crispgame.component';
import { KillbirdsComponent } from './killbirds/killbirds.component';
import { SnakesladdersComponent } from './snakesladders/snakesladders.component';
const routes: Routes = [
  {path:'', component:HomepageComponent},
  {path:'homepage',
  children:[
    {path :'', component: HomepageComponent},
    {path : 'register', component: RegisterComponent},
    {path : 'login', component: LoginComponent}
  ]},
  {path:'register',
  children:[
    {path:'', component:RegisterComponent},
    {path:'curriculum', component:CurriculumComponent},
  ]
} ,
{path:'login',
children:[
  {path:'', component:LoginComponent},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
{path:'curriculum',
children:[
  {path:'', component:CurriculumComponent},
  {path:'gradepage', component:GradepageComponent},
  {path:'extracurricular', component:ExtraCurricularComponent},
  {path:'gamepage', component:GamepageComponent},
]
} ,
{path:'gradepage',
children:[
  {path:'', component:GradepageComponent},
  {path:'grade1', component:Grade1Component},
  {path:'grade2', component:Grade2Component},
  {path:'grade3', component:Grade3Component},
  {path:'grade4', component:Grade4Component},
  {path:'grade5', component:Grade5Component},
]
} ,
{path:'extracurricular',
children:[
  {path:'', component:ExtraCurricularComponent},
  {path:'curriculum', component:CurriculumComponent},
  {path:'#artcraft', component:ExtraCurricularComponent},
  {path:'#drawing', component:ExtraCurricularComponent},
  {path:'#morals', component:ExtraCurricularComponent},
  {path:'#singing', component:ExtraCurricularComponent},
  {path:'#dancing', component:ExtraCurricularComponent},
]
} ,
{path:'grade1',
children:[
  {path:'', component:Grade1Component},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
{path:'grade2',
children:[
  {path:'', component:Grade2Component},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
{path:'grade3',
children:[
  {path:'', component:Grade3Component},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
{path:'grade4',
children:[
  {path:'', component:Grade4Component},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
{path:'grade5',
children:[
  {path:'', component:Grade5Component},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
{path:'gamepage',
children:[
  {path:'', component:GamepageComponent},
  {path:'crispgame', component:CrispgameComponent},
  {path:'killbirds', component:KillbirdsComponent},
  {path:'snakesladders', component:SnakesladdersComponent},
  {path:'curriculum', component:CurriculumComponent},

]
} ,
{path:'killbirds',
children:[
  {path:'', component:KillbirdsComponent},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
{path:'crispgame',
children:[
  {path:'', component:CrispgameComponent},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
{path:'snakesladders',
children:[
  {path:'', component:SnakesladdersComponent},
  {path:'curriculum', component:CurriculumComponent},
]
} ,
  {path:'register', component:RegisterComponent},
  {path:'login', component:LoginComponent},
  {path:'curriculum', component:CurriculumComponent},
  {path:'extracurricular', component:ExtraCurricularComponent},
  {path:'gradepage', component:GradepageComponent},
  {path:'grade1', component:Grade1Component},
  {path:'grade2', component:Grade2Component},
  {path:'grade3', component:Grade3Component},
  {path:'grade4', component:Grade4Component},
  {path:'grade5', component:Grade5Component},
  {path:'gamepage', component:GamepageComponent},
  {path:'crispgame', component:CrispgameComponent},
  {path:'killbirds', component:KillbirdsComponent},
  {path:'snakesladders', component:SnakesladdersComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
