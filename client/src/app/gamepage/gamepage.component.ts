import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gamepage',
  templateUrl: './gamepage.component.html',
  styleUrls: ['./gamepage.component.css']
})
export class GamepageComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }
  
  back(){
    this.router.navigate(["/curriculum"]);
  }

}
