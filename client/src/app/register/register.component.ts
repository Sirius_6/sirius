import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UseropService} from '../userop.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  user:any;
  constructor(private httpClient: HttpClient, private service:UseropService,private router: Router) { 
    this.user={userName:'',email:'',password:'',childName:'',grade:'',mobileNumber:''};
  }

  ngOnInit(): void {
    console.log("data Recieved!!!");
  }
  registerUser(){
    this.service.registerUser(this.user).subscribe();
    this.router.navigate(['/mainpage']);
  }
}
