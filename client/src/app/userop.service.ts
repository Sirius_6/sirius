import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UseropService {

  constructor(private httpClient: HttpClient,) { 
  }
  
  fetchDetails(){
    return this.httpClient.get('http://localhost:3000/fetch');
  }
  registerUser(user:any){
    return this.httpClient.post('http://localhost:3000/register',user);
  }
  getEmployeeByEmailAndPassword(loginForm:any){
    return this.httpClient.get('http://localhost:3000/login/'+loginForm.email+"/"+loginForm.password).toPromise()
  }
  deleteUser(user:any){
    return this.httpClient.delete("http://localhost:3000/delete/"+user.email);
  }
}
