import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-crispgame',
  templateUrl: './crispgame.component.html',
  styleUrls: ['./crispgame.component.css']
})
export class CrispgameComponent implements OnInit {

  constructor(private router : Router) { }

  ngOnInit(): void {
  }
  back(){
    this.router.navigate(["/curriculum"]);
  }
}
