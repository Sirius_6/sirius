import { Component, OnInit } from '@angular/core';
import {UseropService} from '../userop.service';

@Component({
  selector: 'app-showuser',
  templateUrl: './showuser.component.html',
  styleUrls: ['./showuser.component.css']
})
export class ShowuserComponent implements OnInit {
  users:any;
  constructor(private service:UseropService) {
    
  }
//for fetching data and displying on screen
  ngOnInit(): void {
    this.service.fetchDetails().subscribe((result:any)=>{
      this.users=result;
    });
  }
  deleteUser(user : any){
    this.service.deleteUser(user).subscribe((result:any)=>{
      const i = this.users.findIndex((element: any)=>{
        return user.email === element.email;
      })    
      //console.log(employee);
      this.users.splice(i,1); //splice is a method in js to remove 
    });
  }
  

}
