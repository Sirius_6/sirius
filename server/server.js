//node start execution from server.js file wch is acting as main server
//this file used to coll1brate the modules
//@login @register @update @delete


let express = require("express"); //importing express module
let bodyparser = require("body-parser"); // import body-parser
//bodyparser - client info is converted into json
let app = express(); //creating rest object
//enables port communication
//set the JSON as MIME Type
let cors=require("cors");
app.use(cors());
app.use(bodyparser.json());
//read json
app.use(bodyparser.urlencoded({extended:false}));
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "GET , PUT , POST , DELETE");
    res.header("Access-Control-Allow-Headers", "Content-Type, x-requested-with");
    next(); // Important
})
//use login module
// app.use("/login",require("./login/login"));
// //use register module
 app.use("/register",require("./register/register"));
// //use update module
// //app.use("/update",require("./update/update"));
// //use delete module
 app.use("/delete",require("./delete/delete"));
// //use fetch module
app.use("/fetch",require("./fetch/fetch"));

//assingning the port number
app.listen(3000);
console.log("server listening the port no :3000");
